﻿Field=#~Integer~#~50|Field=Designator~String~Designator~390|Field=Center-X(mm)~String~Center-X(mm)~198|Field=Center-Y(mm)~String~Center-Y(mm)~192|Field=Ref-X(mm)~String~Ref-X(mm)~138|Field=Ref-Y(mm)~String~Ref-Y(mm)~129|Field=Pad-X(mm)~String~Pad-X(mm)~92|Field=Pad-Y(mm)~String~Pad-Y(mm)~115|Field=Layer~String~Layer~76|Field=Rotation~String~Rotation~46|ReportField=Currency~<none>|ReportField=ProductionQuantity~1|ReportField=ProjectFileName~ha_v2r4-production_release.PrjPCB|ReportField=ProjectFullPath~C:\Users\johnp\Google Drive\HabitAware\Hardware\repos\hw-mini\HA_v2.0\ha_v2r4-production_release.PrjPCB|ReportField=Title~Pick and Place Report For Project [ha_v2r4-production_release.PrjPCB] (PCB Document : ha_v2r4-production_release.PcbDoc)|ReportField=TotalQuantity~79|ReportField=ReportTime~3:18:06 PM|ReportField=ReportDate~12/9/2016|ReportField=ReportDateTime~12/9/2016 3:18:06 PM|ReportField=OutputName~Pick and Place Report|ReportField=OutputType~BOM_PartType|ReportField=GeneratorName~BOM|ReportField=GeneratorDescription~Bill of Materials|ReportField=VariantName~None|ReportField=Address1~2868 Kenwood Isles Drive|ReportField=Address2~Minneapolis, MN 55408|ReportField=Address3~USA|ReportField=Address4~|ReportField=Application_BuildNumber~|ReportField=ApprovedBy~|ReportField=Author~John Pritchard|ReportField=CheckedBy~|ReportField=CompanyName~|ReportField=CurrentDate~12/9/2016|ReportField=CurrentTime~2:35:23 PM|ReportField=Date~|ReportField=DocumentFullPathAndName~C:\Users\johnp\Google Drive\HabitAware\Hardware\repos\hw-mini\HA_v2.0\ha_v2r4-production_release.SchDoc|ReportField=DocumentName~ha_v2r4-production_release.SchDoc|ReportField=DocumentNumber~|ReportField=DocumentSize~A4|ReportField=DrawnBy~|ReportField=Engineer~|ReportField=ImagePath~|ReportField=ModifiedDate~11/17/2016|ReportField=Organization~HabitAware|ReportField=PCAName~nRF52832-CHAA Reference Layout (DCDC)|ReportField=PCBRevision~|ReportField=ProjectName~ha_v2r4-production_release.PrjPCB|ReportField=ProjectNumber~|ReportField=Revision~|ReportField=Rule~|ReportField=SheetNumber~|ReportField=SheetTotal~|ReportField=Time~|ReportField=Title~|ReportField=DataSourceFileName~ha_v2r4-production_release.PrjPCB|ReportField=DataSourceFullPath~C:\Users\johnp\Google Drive\HabitAware\Hardware\repos\hw-mini\HA_v2.0\ha_v2r4-production_release.PrjPCB|ReportField=PCBDataSourceFileName~ha_v2r4-production_release.PcbDoc|ReportField=PCBDataSourceFullPath~C:\Users\johnp\Google Drive\HabitAware\Hardware\repos\hw-mini\HA_v2.0\ha_v2r4-production_release.PcbDoc
1|ANT1|1.925|6.05|1.925|6.05|1.925|2.55|Top|90
2|C1, C2|6.1, 1.8|1.575, 0.85|6.1, 1.8|1.575, 0.85|6.1, 2.3|2.075, 0.85|Top|270, 180
3|C3, C16|4.539, 3.65|9, 5.9|4.539, 3.65|9, 5.9|4.539, 3.65|9.5, 5.4|Top|270, 90
4|C4, C5, C6, C14, C15|11.6, 8.95, 26.5, 5.675, 26|2.127, 11.375, 10.2, 2.975, 11.45|11.6, 8.95, 26.5, 5.675, 26|2.127, 11.375, 10.2, 2.975, 11.45|11.6, 8.45, 26.5, 6.175, 26.5|1.627, 11.375, 10.7, 2.975, 11.45|Top|90, 0, 270, 180, 180
5|C7|4.35|5.9|4.35|5.9|4.35|6.4|Top|270
6|C8|10.669|1.852|10.669|1.852|10.669|2.527|Top|270
7|C9, C11, C12|7.806, 25, 32.15|2.127, 6.55, 1.375|7.806, 25, 32.15|2.127, 6.55, 1.375|7.806, 25, 32.15|1.627, 6.05, 1.875|Top|90, 90, 270
8|C10, C13|28.7, 22.55|6.625, 4.025|28.7, 22.55|6.625, 4.025|28.7, 22.05|7.125, 4.025|Top|270, 0
9|C19|7.025|2.125|7.025|2.125|7.025|1.625|Top|90
10|D1, D3|30.225, 31.25|10.525, 9.95|30.225, 31.25|10.525, 9.95|30.225, 31.25|10.15, 9.575|Top|0
11|D2|27.8|6.725|27.8|6.725|27.8|6.35|Top|0
12|D4, D5|21.85, 10.95|1.4, 11.325|21.85, 10.95|1.4, 11.325|21.85, 10.575|1.025, 11.325|Top|0, 270
13|FB1|22.55|3|22.55|3|22.075|3|Top|0
14|J1|16.5|3.725|16.5|2.9|15.2|4.95|Top|0
15|L1|4.089|7.25|4.089|7.25|4.564|7.25|Top|180
16|L2|9.556|1.852|9.556|1.852|9.556|1.177|Top|90
17|L3|8.594|2.126|8.594|2.126|8.594|2.601|Top|270
18|P1, P5, P6, P8|31.25, 26.55, 22.475, 27.375|10.85, 1.275, 1.275, 10.85|31.25, 26.55, 22.475, 27.375|10.85, 1.275, 1.275, 10.85|31.25, 26.55, 22.475, 27.375|10.85, 1.275, 1.275, 10.85|Bottom|270, 90, 270, 270
19|Q1|28.95|9.775|28.95|9.725|28.725|9.425|Top|0
20|Q2|27.32|8.275|27.275|8.275|27.66|8.1|Top|90
21|R1, R2|13.675, 12.975|6.875|13.675, 12.975|6.875|13.675, 12.975|7.375|Top|270
22|R3|7|11.35|7|11.35|7.5|11.35|Top|180
23|R4|31.075|8.575|31.075|8.575|30.575|8.575|Top|0
24|R5|29.175|8.6|29.175|8.6|28.675|8.6|Top|0
25|R6, R9, R13, R14, R15, R18|23.625, 28, 12.125, 5.025, 26.75, 20.2|1.45, 10.075, 10.8, 11.25, 6.55, 7.3|23.625, 28, 12.125, 5.025, 26.75, 20.2|1.45, 10.075, 10.8, 11.25, 6.55, 7.3|23.625, 28, 12.125, 5.525, 26.75, 19.7|0.95, 10.575, 10.3, 11.25, 7.05, 7.3|Top|90, 270, 90, 180, 270, 0
26|R7|25.875|6.55|25.875|6.55|25.875|7.05|Top|270
27|R8|22.775|1.425|22.775|1.425|22.775|0.925|Top|90
28|R10|29.025|1.375|29.025|1.375|29.025|0.875|Top|90
29|R11|31.083|1.375|31.083|1.375|31.083|1.875|Top|270
30|R12|30.217|1.375|30.217|1.375|30.217|0.875|Top|90
31|R16|32.25|9.9|32.25|9.9|32.25|10.4|Top|270
32|SW1|16.5|10|16.5|10|13.55|10|Top|0
33|TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, TP15, TP16, TP17, TP18, TP19|24.925, 2.675, 5.25, 26.575, 9.35, 30.425, 17.2, 14.675, 4.675, 22.025, 5.475, 20.6, 30.6, 13.825, 17.325, 6.125, 3.675, 9.825, 7.475|7.3, 1.575, 5.325, 6.325, 1.4, 3.325, 7.5, 8.45, 8.325, 7.725, 10.675, 5.75, 6.15, 10.475, 5.8, 2.575, 3.45, 3.775, 10.4|24.925, 2.675, 5.25, 26.575, 9.35, 30.425, 17.2, 14.675, 4.675, 22.025, 5.475, 20.6, 30.6, 13.825, 17.325, 6.125, 3.675, 9.825, 7.475|7.3, 1.575, 5.325, 6.325, 1.4, 3.325, 7.5, 8.45, 8.325, 7.725, 10.675, 5.75, 6.15, 10.475, 5.8, 2.575, 3.45, 3.775, 10.4|24.925, 2.675, 5.25, 26.575, 9.35, 30.425, 17.2, 14.675, 4.675, 22.025, 5.475, 20.6, 30.6, 13.825, 17.325, 6.125, 3.675, 9.825, 7.475|7.3, 1.575, 5.325, 6.325, 1.4, 3.325, 7.5, 8.45, 8.325, 7.725, 10.675, 5.75, 6.15, 10.475, 5.8, 2.575, 3.45, 3.775, 10.4|Bottom|90, 180, 180, 90, 90, 180, 90, 90, 180, 180, 180, 90, 90, 90, 90, 90, 90, 90, 90
34|U1|8.664|7.038|8.664|7.038|11.689|4.838|Top|180
35|U2|26.35|3.175|26.35|3.2|25.6|1.7|Top|90
36|U3|23.165|9.68|23.165|9.68|22.165|8.155|Top|90
37|U4|30.9|6.225|30.9|6.225|32.075|5.275|Top|90
38|U5|30.525|3.5|30.525|3.5|31.445|2.85|Top|180
39|U6|16.5|6.875|16.5|6.875|17.913|6.125|Top|180
40|X1|4.275|1.5|4.275|1.5|3.55|0.925|Top|0
41|X2|21.2|5.625|21.2|5.625|21.2|6.35|Top|270
